package de.codecamp.eclipse.javabuildpathsorter.commands;


import de.codecamp.eclipse.javabuildpathsorter.util.ClasspathUtils;
import java.util.stream.StreamSupport;
import javax.inject.Named;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Evaluate;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jface.viewers.IStructuredSelection;


/**
 * <b>Warning</b> : As explained in <a href=
 * "http://wiki.eclipse.org/Eclipse4/RCP/FAQ#Why_aren.27t_my_handler_fields_being_re-injected.3F">this
 * wiki page</a>, it is not recommended to define @Inject fields in a handler. <br/>
 * <br/>
 * <b>Inject the values in the @Execute methods</b>
 */
public class SortJavaBuildPathHandler
{

  @CanExecute
  @Evaluate
  public boolean canExecute(
      @Named(IServiceConstants.ACTIVE_SELECTION) IStructuredSelection selection)
  {
    @SuppressWarnings("unchecked")
    Iterable<Object> iterable = selection;
    return StreamSupport.stream(iterable.spliterator(), false).anyMatch(item ->
    {
      if (item instanceof IProject project)
      {
        return JavaCore.create(project).exists();
      }
      else
      {
        return item instanceof IJavaProject;
      }
    });
  }

  @Execute
  public void execute(@Named(IServiceConstants.ACTIVE_SELECTION) IStructuredSelection selection,
      IProgressMonitor monitor)
    throws CoreException
  {
    for (Object element : selection.toArray())
    {
      IJavaProject javaProject;
      if (element instanceof IProject project)
      {
        javaProject = JavaCore.create(project);
        if (!javaProject.exists())
          continue;
      }
      else if (element instanceof IJavaProject)
      {
        javaProject = (IJavaProject) element;
      }
      else
      {
        continue;
      }

      ClasspathUtils.sort(javaProject, monitor);
    }
  }

}
