package de.codecamp.eclipse.javabuildpathsorter.util;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;


public class ClasspathUtils
{

  private ClasspathUtils()
  {
    // utility class
  }


  public static void sort(IJavaProject javaProject, IProgressMonitor monitor)
    throws CoreException
  {
    List<IClasspathEntry> originalClasspathEntries = Arrays.asList(javaProject.getRawClasspath());
    List<IClasspathEntry> unpickedClasspathEntries = new ArrayList<>(originalClasspathEntries);
    List<IClasspathEntry> newClasspathEntries = new ArrayList<>();

    pickSourceEntry(javaProject.getProject(), unpickedClasspathEntries, "src")
        .ifPresent(newClasspathEntries::add);
    pickSourceEntry(javaProject.getProject(), unpickedClasspathEntries, "src/main/java")
        .ifPresent(newClasspathEntries::add);
    newClasspathEntries.addAll(pickSourceEntries(javaProject.getProject(), unpickedClasspathEntries,
        "target/generated-sources"));
    pickSourceEntry(javaProject.getProject(), unpickedClasspathEntries, "src/main/resources")
        .ifPresent(newClasspathEntries::add);

    pickSourceEntry(javaProject.getProject(), unpickedClasspathEntries, "src/test/java")
        .ifPresent(newClasspathEntries::add);
    newClasspathEntries.addAll(pickSourceEntries(javaProject.getProject(), unpickedClasspathEntries,
        "target/generated-test-sources"));
    pickSourceEntry(javaProject.getProject(), unpickedClasspathEntries, "src/test/resources")
        .ifPresent(newClasspathEntries::add);

    newClasspathEntries.addAll(pickLibraries(unpickedClasspathEntries));

    newClasspathEntries.addAll(
        pickContainer(unpickedClasspathEntries, "org.eclipse.m2e.MAVEN2_CLASSPATH_CONTAINER"));
    newClasspathEntries
        .addAll(pickContainer(unpickedClasspathEntries, "org.eclipse.pde.core.requiredPlugins"));
    newClasspathEntries
        .addAll(pickContainer(unpickedClasspathEntries, "org.eclipse.jdt.launching.JRE_CONTAINER"));

    Collections.sort(unpickedClasspathEntries,
        Comparator.comparing(item -> item.getPath().toString()));
    newClasspathEntries.addAll(unpickedClasspathEntries);

    if (!newClasspathEntries.equals(originalClasspathEntries))
    {
      javaProject.setRawClasspath(newClasspathEntries.toArray(IClasspathEntry[]::new), true,
          monitor);
    }
  }

  private static Optional<IClasspathEntry> pickSourceEntry(IProject project,
      List<IClasspathEntry> classpathEntries, String path)
  {
    for (Iterator<IClasspathEntry> it = classpathEntries.iterator(); it.hasNext();)
    {
      IClasspathEntry entry = it.next();
      if (entry.getEntryKind() == IClasspathEntry.CPE_SOURCE)
      {
        IPath p = entry.getPath();
        if (p.segment(0).equals(project.getName()))
          p = p.removeFirstSegments(1);

        if (p.toPortableString().equals(path))
        {
          it.remove();
          return Optional.of(entry);
        }
      }
    }
    return Optional.empty();
  }

  private static List<IClasspathEntry> pickSourceEntries(IProject project,
      List<IClasspathEntry> classpathEntries, String pathPrefix)
  {
    List<IClasspathEntry> result = new ArrayList<>();
    for (Iterator<IClasspathEntry> it = classpathEntries.iterator(); it.hasNext();)
    {
      IClasspathEntry entry = it.next();
      if (entry.getEntryKind() == IClasspathEntry.CPE_SOURCE)
      {
        IPath p = entry.getPath();
        if (p.segment(0).equals(project.getName()))
          p = p.removeFirstSegments(1);

        if (p.toPortableString().equals(pathPrefix)
            || p.toPortableString().startsWith(pathPrefix + "/"))
        {
          result.add(entry);
          it.remove();
        }
      }
    }
    Collections.sort(result, Comparator.comparing(item -> item.getPath().toString()));
    return result;
  }

  private static List<IClasspathEntry> pickLibraries(List<IClasspathEntry> classpathEntries)
  {
    List<IClasspathEntry> result = new ArrayList<>();
    for (Iterator<IClasspathEntry> it = classpathEntries.iterator(); it.hasNext();)
    {
      IClasspathEntry entry = it.next();
      if (entry.getEntryKind() == IClasspathEntry.CPE_LIBRARY)
      {
        result.add(entry);
        it.remove();
      }
    }
    Collections.sort(result, Comparator.comparing(item -> item.getPath().toString()));
    return result;
  }

  private static List<IClasspathEntry> pickContainer(List<IClasspathEntry> classpathEntries,
      String name)
  {
    List<IClasspathEntry> result = new ArrayList<>();
    for (Iterator<IClasspathEntry> it = classpathEntries.iterator(); it.hasNext();)
    {
      IClasspathEntry entry = it.next();
      if (entry.getEntryKind() == IClasspathEntry.CPE_CONTAINER)
      {
        if (entry.getPath().segment(0).equals(name))
        {
          result.add(entry);
          it.remove();
        }
      }
    }
    Collections.sort(result, Comparator.comparing(item -> item.getPath().toString()));
    return result;
  }

}
