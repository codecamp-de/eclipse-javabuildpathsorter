package de.codecamp.eclipse.javabuildpathsorter.config;


import de.codecamp.eclipse.javabuildpathsorter.util.ClasspathUtils;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.m2e.core.project.configurator.AbstractProjectConfigurator;
import org.eclipse.m2e.core.project.configurator.ProjectConfigurationRequest;


public class MavenProjectConfigurator
  extends
    AbstractProjectConfigurator
{

  @Override
  public void configure(ProjectConfigurationRequest request, IProgressMonitor monitor)
    throws CoreException
  {
    IJavaProject javaProject = JavaCore.create(request.mavenProjectFacade().getProject());
    if (!javaProject.exists())
      return;

    ClasspathUtils.sort(javaProject, monitor);
  }

}
