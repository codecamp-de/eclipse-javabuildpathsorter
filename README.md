# Eclipse Build Path Sorter

*Build Path Sorter* is a plug-in for the Eclipse IDE that sorts the entries of the Java build path because they practically always end up in an inconsistent order.
The build path is sorted automatically after configuring a Maven project, but it can also be triggered manually via the `Configure` context menu on Java projects.

This is the order *Build Path Sorter* will sort the entries in:

- src
- src/main/java
- target/generated-sources/*
- src/main/resources


- src/test/java
- target/generated-test-sources/*
- src/test/resources


- `Referenced Libraries`
- `Maven Dependencies`
- `Plug-in Dependencies`
- `JRE System Library`


- the remaining entries sorted lexicographically by their path


## Installation
- **Releases:** https://codecamp.de/~p2/eclipse-javabuildpathsorter/
- **Latest Snapshot:** https://codecamp-de.gitlab.io/eclipse-javabuildpathsorter/snapshot/
